package gopol

import (
	"fmt"
	"image/color"
)

var ErrNotAvailable = fmt.Errorf("data not available")

type VotingSystem string

const (
	FPTP VotingSystem = "fptp"
	IRV  VotingSystem = "irv"
	STV  VotingSystem = "stv"
	PR   VotingSystem = "pr"
	MMP  VotingSystem = "mmp"
)

type Chamber struct {
	Name   string
	Voting VotingSystem
}

// Type Legislature represents a
// System's legislature.
//
// In a bicameral system,
// the lower Chamber should
// come before the upper Chamber.
type Legislature struct {
	Name     string
	Chambers []Chamber // Chambers
}

type Executive struct {
	Name   string
	Voting VotingSystem
}

type SystemType string

const (
	Presidential     SystemType = "presidential"
	Parliamentary    SystemType = "parliamentary"
	SemiPresidential SystemType = "semi-presidential"
)

// Type System represents a
// democratic system of
// government.
type System struct {
	Legislature Legislature
	Executive   []Executive
	Type        SystemType
}

type Party struct {
	Name      string
	ShortName string
	Color     color.Color
}

type Politician struct {
	Name string

	// 1- or 2-letter abbreviation
	// denoting the party.
	Party string
}

type Vote int8

const (
	Abstain Vote = 0
	No      Vote = -1
	Yes     Vote = 1
)

type Bill interface {
	Name() string
	Votes() []map[Politician]Vote
	ContentURL() string
	Session() int
}

// Type LegislatureSession represents
// a session of the legislature.
//
// An implementor may additionally define:
//
//     Coalitions() [][]string
//     District(pol Politician) string
//
type LegislatureSession interface {
	Members(body int) []Politician
}

// Function Coalition returns
// the coalitions of a specific legislature.
func Coalitions(l LegislatureSession) [][]string {
	lc, ok := l.(interface{ Coalitions() [][]string })
	if !ok {
		return nil
	}
	return lc.Coalitions()
}

type districtLegislature interface {
	LegislatureSession
	District(pol Politician) string
}

// Function District returns
// the distrct that politician
// in a legislature represents, or
// an empty string for members elected from
// a party-list.
func District(l LegislatureSession, pol Politician) string {
	if l2, ok := l.(districtLegislature); ok {
		return l2.District(pol)
	}

	return ""
}

type ExecutiveSession interface {
	Member() Politician
}

type Interface interface {
	System() *System

	Party(abbr string) *Party

	Legislature(session int) (LegislatureSession, error)
	Executive(session int) (ExecutiveSession, error)

	// Returns the bill and the legislature, or
	// nil, -1 if an error occurred
	Bill(id string) (Bill, error)
}
