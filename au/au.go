package au

import (
	"bitbucket.org/allenb123/gopol"
	"bufio"
	"encoding/csv"
	"fmt"
	"golang.org/x/net/html"
	"image/color"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

var system = gopol.System{
	Type: gopol.Parliamentary,
	Legislature: gopol.Legislature{
		Name: "Parliament",
		Chambers: []gopol.Chamber{
			{"House of Representatives", gopol.IRV},
			{"Senate", gopol.STV},
		},
	},
	Executive: []gopol.Executive{
		{"Prime Minister", ""},
	},
}

var parties = map[string]*gopol.Party{
	"LP":  &gopol.Party{"Liberal Party", "Liberal", color.RGBA{41, 76, 155, 255}},
	"LNP": &gopol.Party{"Liberal National", "Liberal-National", color.RGBA{31, 78, 142, 255}},
	"NP":  &gopol.Party{"National Party", "National", color.RGBA{35, 125, 72, 255}},
	"CLP": &gopol.Party{"Country Liberal Party", "Country", color.RGBA{248, 115, 28, 255}},

	"ALP": &gopol.Party{"Australian Labor Party", "Labor", color.RGBA{218, 46, 67, 255}},
	"GRN": &gopol.Party{"Australian Greens", "Greens", color.RGBA{0, 156, 61, 255}},

	"XEN": &gopol.Party{"Centre Alliance", "Centre", color.RGBA{247, 82, 26, 255}},
	"ON":  &gopol.Party{"Pauline Hanson's One Nation", "One Nation", color.RGBA{236, 106, 43, 255}},
	"LDP": &gopol.Party{"Liberal Democratic Party", "Liberal Democratic", color.RGBA{249, 199, 49, 255}},
}

type interface_ struct {
	legislatures map[int]legislature
}

func New() gopol.Interface {
	i := new(interface_)
	i.legislatures = make(map[int]legislature)
	return i
}

func (i *interface_) System() *gopol.System {
	return &system
}

func (i *interface_) Party(abbr string) *gopol.Party {
	return parties[abbr]
}

type legislature struct {
	members [2]map[gopol.Politician]string
}

func (l legislature) Members(body int) []gopol.Politician {
	out := make([]gopol.Politician, 0, len(l.members[body]))
	for member, _ := range l.members[body] {
		out = append(out, member)
	}
	return out
}

func (l legislature) District(p gopol.Politician) string {
	if res, ok := l.members[0][p]; ok {
		return res
	}
	return l.members[1][p]
}

func (l legislature) Coalitions() [][]string {
	// TODO: like, actual coalitions
	// not sure where to get this info from though
	return [][]string{
		{"LP", "LNP", "NP", "CLP"},
	}
}

func (i *interface_) Legislature(session int) (gopol.LegislatureSession, error) {
	if res, ok := i.legislatures[session]; ok {
		return res, nil
	}

	year := 1881 + 3*session

	electionURL := fmt.Sprintf("https://www.aec.gov.au/Elections/Federal_Elections/%d/index.htm", year)
	resp, err := http.Get(electionURL)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != 200 {
		return nil, gopol.ErrNotAvailable
	}

	root, err := html.Parse(resp.Body)
	if err != nil {
		return nil, err
	}

	getAttr := func(n *html.Node, attr string) string {
		for _, attrdata := range n.Attr {
			if attrdata.Key == attr {
				return attrdata.Val
			}
		}
		return ""
	}

	var findAnchor func(n *html.Node) (*html.Node, bool)
	findAnchor = func(n *html.Node) (*html.Node, bool) {
		if n.Type == html.ElementNode && n.Data == "a" && n.FirstChild != nil && n.FirstChild.Type == html.TextNode {
			lower := strings.ToLower(strings.Trim(n.FirstChild.Data, " \t\n"))
			if strings.Contains(lower, "official results") || strings.Contains(lower, fmt.Sprint(year)+" federal election results") {
				return n, true
			}
		}

		for c := n.FirstChild; c != nil; c = c.NextSibling {
			if res, ok := findAnchor(c); ok {
				return res, true
			}
		}
		return nil, false
	}

	a, ok := findAnchor(root)
	if !ok {
		return nil, gopol.ErrNotAvailable
	}

	href := getAttr(a, "href")
	u, err := url.Parse(href)
	if err != nil {
		return nil, fmt.Errorf("couldn't find valid <a> tag: %v", err)
	}
	aecIdStr := strings.Split(u.Path, "/")[1]

	aecId, err := strconv.Atoi(aecIdStr)
	if err != nil {
		cl := &http.Client{}
		resp, err := cl.Get("https://www.aec.gov.au/results")
		aecIdStr := strings.Split(resp.Request.URL.Path, "/")[1]
		aecId, err = strconv.Atoi(aecIdStr)
		if err != nil {
			return nil, fmt.Errorf("couldn't find valid <a> tag: %v", err)
		}
	}
	houseURL := fmt.Sprintf("https://results.aec.gov.au/%d/Website/Downloads/HouseMembersElectedDownload-%d.csv", aecId, aecId)
	senateURL := fmt.Sprintf("https://results.aec.gov.au/%d/Website/Downloads/SenateSenatorsElectedDownload-%d.csv", aecId, aecId)
	chambersURL := []string{houseURL, senateURL}

	res := legislature{}
	for i, url := range chambersURL {
		resp, err := http.Get(url)
		if err != nil {
			return nil, err
		}

		reader := bufio.NewReader(resp.Body)
		reader.ReadString('\n')
		reader.ReadString('\n')

		res.members[i] = make(map[gopol.Politician]string)

		r := csv.NewReader(reader)
		for {
			record, err := r.Read()
			if err == io.EOF {
				break
			} else if err != nil {
				return nil, err
			}

			name := ""
			district := ""
			party := ""
			if i == 0 {
				// House
				name = record[4] + " " + strings.Title(strings.ToLower(record[5]))
				district = record[2] + "-" + record[1]
				party = record[7]
			} else {
				// Senate
				name = record[1] + " " + strings.Title(strings.ToLower(record[2]))
				party = record[4]
				district = record[0]
			}
			if party == "IND" {
				party = ""
			}
			if party == "GVIC" {
				party = "GRN"
			}
			pol := gopol.Politician{Name: name, Party: party}
			res.members[i][pol] = district
		}
	}

	i.legislatures[session] = res
	return res, nil
}

func (i *interface_) Executive(session int) (gopol.ExecutiveSession, error) {
	panic("TODO: unimplemented")
}

func (i *interface_) Bill(id string) (gopol.Bill, error) {
	panic("TODO: unimplemented")
}
