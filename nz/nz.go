package nz

import (
	"bitbucket.org/allenb123/gopol"
	"fmt"
	"golang.org/x/net/html"
	"image/color"
	"net/http"
	"strings"
	"unicode"
)

type interface_ struct{}

func New() gopol.Interface {
	c := new(interface_)
	return c
}

var system = gopol.System{
	Legislature: gopol.Legislature{
		Name: "Parliament",
		Chambers: []gopol.Chamber{{
			Name:   "House of Representatives",
			Voting: gopol.MMP,
		}},
	},
	Executive: []gopol.Executive{{
		Name: "Prime Minister",
	}},
}

func (c *interface_) System() *gopol.System {
	return &system
}

var parties = map[string]*gopol.Party{
	"ANZ": &gopol.Party{"ACT New Zealand", "ACT", color.RGBA{251, 225, 33, 255}},
	"L":   &gopol.Party{"Labour Party", "Labour", color.RGBA{216, 45, 32, 255}},
	"N":   &gopol.Party{"National Party", "National", color.RGBA{53, 128, 195, 255}},
	"G":   &gopol.Party{"Green Party", "Green", color.RGBA{85, 193, 76, 255}},
	"NZF": &gopol.Party{"New Zealand First", "NZ First", color.RGBA{64, 64, 65, 255}},
	"M":   &gopol.Party{"Māori Party", "Māori", color.RGBA{210, 0, 35, 255}},
	"UF":  &gopol.Party{"United Future New Zealand", "United Future", color.RGBA{73, 0, 78, 255}},
}

func (c *interface_) Party(abbr string) *gopol.Party {
	return parties[abbr]
}

var coalitions = map[int][][]string{
	45: {{"N", "NZF"}},
	46: {{"L", "A", "G"}},
	47: {{"L", "P", "UF"}},
	48: {{"L", "P", "NZF", "UF"}},
	49: {{"N", "ANZ", "UF", "M"}},
	50: {{"N", "ANZ", "UF", "M"}},
	51: {{"N", "ANZ", "UF", "M"}},
	52: {{"L", "NZF", "G"}},
}

type legislatureSession struct {
	session int
	members map[gopol.Politician]string
}

func (l legislatureSession) Members(body int) []gopol.Politician {
	out := make([]gopol.Politician, 0, len(l.members))
	for member, _ := range l.members {
		out = append(out, member)
	}
	return out
}

func (l legislatureSession) District(pol gopol.Politician) string {
	return l.members[pol]
}

func (l legislatureSession) Coalitions() [][]string {
	return coalitions[l.session]
}

func htmlGatherText(n *html.Node) string {
	if n.Type == html.TextNode {
		return n.Data
	}

	res := ""
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		res += htmlGatherText(c)
	}
	return res
}

func (c *interface_) Legislature(session int) (gopol.LegislatureSession, error) {
	year := 1861 + 3*session

	url := fmt.Sprintf("https://www.electionresults.govt.nz/electionresults_%d/successful-candidates.html", year)
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != 200 {
		url := fmt.Sprintf("https://www.electionresults.govt.nz/electionresults_%d/successfulcand.html", year)
		resp, err = http.Get(url)
		if err != nil {
			return nil, err
		}
		if resp.StatusCode != 200 {
			return nil, gopol.ErrNotAvailable
		}
	}

	root, err := html.Parse(resp.Body)
	if err != nil {
		return nil, err
	}

	getAttr := func(n *html.Node, attr string) string {
		for _, attrdata := range n.Attr {
			if attrdata.Key == attr {
				return attrdata.Val
			}
		}
		return ""
	}

	var findTable func(n *html.Node) (*html.Node, bool)
	findTable = func(n *html.Node) (*html.Node, bool) {
		if n.Type == html.ElementNode && n.Data == "table" && (getAttr(n, "id") == "successful_candidates_table" || getAttr(n, "class") == "eletext") {
			return n, true
		}

		for c := n.FirstChild; c != nil; c = c.NextSibling {
			if res, ok := findTable(c); ok {
				return res, true
			}
		}
		return nil, false
	}

	table, ok := findTable(root)
	if !ok {
		return nil, fmt.Errorf("failed to find <table> in results")
	}

	members := make(map[gopol.Politician]string)

	party := ""
	var executeRow func(n *html.Node)
	executeRow = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "tr" {
			td := n.FirstChild
			for td != nil && !(td.Type == html.ElementNode && td.Data == "td") {
				td = td.NextSibling
			}
			if td == nil {
				return
			}

			h3 := td.FirstChild
			for h3 != nil && !(h3.Data == "h3" && h3.Type == html.ElementNode) {
				h3 = h3.NextSibling
			}

			if h3 != nil || getAttr(td, "colspan") == "3" {
				newparty := strings.TrimSpace(htmlGatherText(td))
				if strings.HasSuffix(newparty, "Party") {
					newparty = strings.TrimSpace(newparty[:len(newparty)-len("Party")])
				}
				if strings.EqualFold(newparty, "Mana") {
					newparty = "MANA"
					return
				}
				fields := strings.Fields(newparty)
				newparty = ""
				for _, word := range fields {
					if unicode.IsLetter(rune(word[0])) {
						newparty += string(word[0])
					}
				}
				newparty = strings.ToUpper(newparty)
				if newparty != "" {
					party = newparty
				}
				return
			}

			name := strings.TrimSpace(htmlGatherText(td))
			fields := strings.Split(name, ",")
			if len(fields) == 2 {
				name = strings.TrimSpace(fields[1]) + " " + strings.TrimSpace(strings.Title(strings.ToLower(fields[0])))
			}

			td = td.NextSibling
			for td != nil && !(td.Type == html.ElementNode && td.Data == "td") {
				td = td.NextSibling
			}
			if td == nil {
				return
			}
			seat := strings.TrimSpace(htmlGatherText(td))
			if strings.EqualFold(seat, "List Seat") {
				seat = ""
			} else if strings.HasSuffix(seat, "Electorate") {
				seat = strings.TrimSpace(seat[:len(seat)-len("Electorate")])
			}

			pol := gopol.Politician{name, party}
			members[pol] = seat
		}

		for c := n.FirstChild; c != nil; c = c.NextSibling {
			executeRow(c)
		}
	}
	executeRow(table)

	return legislatureSession{session, members}, nil
}

func (c *interface_) Executive(session int) (gopol.ExecutiveSession, error) {
	panic("unimplemented")
}

func (c *interface_) Bill(id string) (gopol.Bill, error) {
	panic("unimplemented")
}
