package us

import (
	"bitbucket.org/allenb123/gopol"
	"encoding/xml"
	"fmt"
	"golang.org/x/net/html"
	"image/color"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"unicode"
)

var States = map[string]string{
	"AL": "Alabama",
	"AK": "Alaska",
	"AZ": "Arizona",
	"AR": "Arkansas",
	"CA": "California",
	"CO": "Colorado",
	"CT": "Connecticut",
	"DE": "Delaware",
	"DC": "District of Columbia",
	"FL": "Florida",
	"GA": "Georgia",
	"HI": "Hawaii",
	"ID": "Idaho",
	"IL": "Illinois",
	"IN": "Indiana",
	"IA": "Iowa",
	"KS": "Kansas",
	"KY": "Kentucky",
	"LA": "Louisiana",
	"ME": "Maine",
	"MD": "Maryland",
	"MA": "Massachusetts",
	"MI": "Michigan",
	"MN": "Minnesota",
	"MS": "Mississippi",
	"MO": "Missouri",
	"MT": "Montana",
	"NE": "Nebraska",
	"NV": "Nevada",
	"NH": "New Hampshire",
	"NJ": "New Jersey",
	"NM": "New Mexico",
	"NY": "New York",
	"NC": "North Carolina",
	"ND": "North Dakota",
	"OH": "Ohio",
	"OK": "Oklahoma",
	"OR": "Oregon",
	"PA": "Pennsylvania",
	"RI": "Rhode Island",
	"SC": "South Carolina",
	"SD": "South Dakota",
	"TN": "Tennessee",
	"TX": "Texas",
	"UT": "Utah",
	"VT": "Vermont",
	"VA": "Virginia",
	"WA": "Washington",
	"WV": "West Virginia",
	"WI": "Wisconsin",
	"WY": "Wyoming",
}

var statesRev = make(map[string]string)

var parties = map[string]*gopol.Party{
	"R": &gopol.Party{"Republican Party", "Republican", color.RGBA{224, 0, 40, 255}},
	"D": &gopol.Party{"Democratic Party", "Democratic", color.RGBA{0, 174, 243, 255}},
	"L": &gopol.Party{"Libertarian Party", "Libertarian", color.RGBA{255, 223, 0, 255}},
}

var system = gopol.System{
	Type: gopol.Presidential,
	Legislature: gopol.Legislature{
		Name: "Congress",
		Chambers: []gopol.Chamber{
			{"House of Representatives", gopol.FPTP},
			{"Senate", gopol.FPTP},
		},
	},
	Executive: []gopol.Executive{
		{"President", gopol.FPTP},
	},
}

type legislator struct {
	gopol.Politician
	District string
}

func init() {
	for abbr, state := range States {
		statesRev[state] = abbr
	}
}

type interface_ struct {
	congresses map[int][][]legislator
}

func New() gopol.Interface {
	i := new(interface_)
	i.congresses = make(map[int][][]legislator)
	return i
}

func (i *interface_) System() *gopol.System {
	return &system
}

func (i *interface_) Party(abbr string) *gopol.Party {
	return parties[abbr]
}

func (i *interface_) Bill(id string) (gopol.Bill, error) {
	i1 := -1
	i2 := -1
	for i, ch := range id {
		if i1 == -1 && unicode.IsLetter(ch) {
			i1 = i
			continue
		}
		if i1 != -1 && unicode.IsDigit(ch) {
			i2 = i
			break
		}
	}

	if i1 == -1 || i2 == -1 {
		return nil, fmt.Errorf("invalid bill id")
	}

	congress := id[:i1]
	chamber := id[i1:i2]
	num := id[i2:]

	congressInt, err := strconv.Atoi(congress)
	if err != nil {
		return nil, fmt.Errorf("invalid bill id: %v", err)
	}

	url := fmt.Sprintf("https://www.govinfo.gov/bulkdata/BILLSTATUS/%d/%s/BILLSTATUS-%d%s%s.xml", congressInt, chamber, congressInt, chamber, num)

	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var billRaw billXml
	err = xml.Unmarshal(body, &billRaw)
	if err != nil {
		return nil, err
	}

	bill := new(Bill)
	bill.name = billRaw.Title
	bill.OriginChamber = billRaw.OriginChamber
	bill.contentURL = billRaw.TextVersions[0]

	// Sponsors & Co-sponsors
	for _, sponsor := range billRaw.Sponsors {
		name := sponsor.FirstName + " " + sponsor.LastName
		if sponsor.MiddleName != "" {
			name = sponsor.FirstName + " " + sponsor.MiddleName + " " + sponsor.LastName
		}
		name = strings.Title(strings.ToLower(name))
		party := strings.ToUpper(sponsor.Party)
		if party == "I" {
			party = ""
		}

		politician := gopol.Politician{Name: name, Party: party}
		bill.Sponsors = append(bill.Sponsors, politician)
	}

	for _, sponsor := range billRaw.CoSponsors {
		name := sponsor.FirstName + " " + sponsor.LastName
		if sponsor.MiddleName != "" {
			name = sponsor.FirstName + " " + sponsor.MiddleName + " " + sponsor.LastName
		}
		name = strings.Title(strings.ToLower(name))
		party := strings.ToUpper(sponsor.Party)
		if party == "I" {
			party = ""
		}

		politician := gopol.Politician{Name: name, Party: party}
		bill.CoSponsors = append(bill.CoSponsors, politician)
	}

	votes := make([]map[gopol.Politician]gopol.Vote, 2)
	for _, vote := range billRaw.Votes {
		if vote.Chamber == "House" {
			res, err := i.billHouseVotes(vote.URL, congressInt)
			if err != nil {
				return nil, err
			}
			votes[0] = res
		}
		if vote.Chamber == "Senate" {
			res, err := i.billSenateVotes(vote.URL)
			if err != nil {
				return nil, err
			}
			votes[1] = res
		}
	}
	bill.votes = votes

	bill.session = congressInt

	return bill, nil
}

type Bill struct {
	Sponsors      []gopol.Politician
	CoSponsors    []gopol.Politician
	OriginChamber string

	session    int
	name       string
	votes      []map[gopol.Politician]gopol.Vote
	contentURL string
}

func (b *Bill) Name() string {
	return b.name
}

func (b *Bill) Votes() []map[gopol.Politician]gopol.Vote {
	return b.votes
}

func (b *Bill) ContentURL() string {
	return b.contentURL
}

func (b *Bill) Session() int {
	return b.session
}

type billXml struct {
	OriginChamber string        `xml:"bill>originChamber"`
	Votes         []billVoteXml `xml:"bill>recordedVotes>recordedVote"`
	Title         string        `xml:"bill>title"`
	Sponsors      []sponsorXml  `xml:"bill>sponsors>item"`
	CoSponsors    []sponsorXml  `xml:"bill>cosponsors>item"`
	TextVersions  []string      `xml:"bill>textVersions>item>formats>item>url"`
}

type billVoteXml struct {
	Chamber string `xml:"chamber"`
	URL     string `xml:"url"`
}

type sponsorXml struct {
	Party      string `xml:"party"`
	State      string `xml:"state"`
	FirstName  string `xml:"firstName"`
	MiddleName string `xml:"middleName"`
	LastName   string `xml:"lastName"`
}

func (i *interface_) billHouseVotes(url string, n int) (map[gopol.Politician]gopol.Vote, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var votesRaw houseVotesXml
	err = xml.Unmarshal(body, &votesRaw)
	if err != nil {
		return nil, err
	}

	votes := make(map[gopol.Politician]gopol.Vote)

	congress, err := i.legislature(n)
	if err != nil {
		return nil, err
	}

	house := congress[0]

	for _, vote := range votesRaw.Votes {
		name := vote.Legislator.Name
		party := vote.Legislator.Party
		state := vote.Legislator.State

		if strings.Contains(name, "(") {
			i := strings.Index(name, "(")
			name = strings.Trim(name[:i], " \t")
		}

		// Normalize names
		if strings.Contains(name, ",") {
			nameParts := strings.Split(name, ",")
			name = strings.Trim(nameParts[1], " \t") + " " + strings.Trim(nameParts[0], " \t")
		} else {
			for _, member := range house {
				if fmt.Sprint(member.District) == state && (strings.HasSuffix(member.Name, name) || strings.HasSuffix(member.Name, name+" Jr.") || strings.HasSuffix(member.Name, name+" Sr.")) {
					name = member.Name
					break
				}
			}
		}

		if party == "I" {
			party = ""
		}

		politician := gopol.Politician{
			Name:  name,
			Party: party,
		}

		votecode := gopol.Abstain
		if vote.Vote == "Yea" {
			votecode = gopol.Yes
		} else if vote.Vote == "Nay" {
			votecode = gopol.No
		}

		votes[politician] = votecode
	}

	return votes, nil
}

type houseVotesXml struct {
	Votes []houseVoteXml `xml:"vote-data>recorded-vote"`
}

type houseVoteXml struct {
	Vote       string          `xml:"vote"`
	Legislator houseLegislator `xml:"legislator"`
}

type houseLegislator struct {
	Name  string `xml:"unaccented-name,attr"`
	Party string `xml:"party,attr"`
	State string `xml:"state,attr"`
}

func (i *interface_) billSenateVotes(url string) (map[gopol.Politician]gopol.Vote, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	root, err := html.Parse(resp.Body)
	if err != nil {
		return nil, err
	}

	var findAnchor func(n *html.Node) (*html.Node, bool)
	findAnchor = func(n *html.Node) (*html.Node, bool) {
		if n.Type == html.ElementNode && n.Data == "a" && n.FirstChild != nil && n.FirstChild.Type == html.TextNode && strings.Trim(n.FirstChild.Data, " \t\n") == "XML" {
			return n, true
		}

		for c := n.FirstChild; c != nil; c = c.NextSibling {
			if res, ok := findAnchor(c); ok {
				return res, true
			}
		}
		return nil, false
	}

	getAttr := func(n *html.Node, attr string) string {
		for _, attrdata := range n.Attr {
			if attrdata.Key == attr {
				return attrdata.Val
			}
		}
		return ""
	}

	anchor, ok := findAnchor(root)
	if !ok {
		return nil, fmt.Errorf("couldn't find anchor to xml")
	}
	xmlUrl := "https://www.senate.gov/" + getAttr(anchor, "href")
	resp, err = http.Get(xmlUrl)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var data senateVoteXml
	err = xml.Unmarshal(body, &data)
	if err != nil {
		return nil, err
	}

	results := make(map[gopol.Politician]gopol.Vote)
	for _, member := range data.Members {
		name := member.FirstName + " " + member.LastName
		party := member.Party
		if party == "I" {
			party = ""
		}

		vote := gopol.Abstain
		if strings.EqualFold(member.Vote, "Yea") {
			vote = gopol.Yes
		} else if strings.EqualFold(member.Vote, "Nay") {
			vote = gopol.No
		}

		results[gopol.Politician{Name: name, Party: member.Party}] = vote
	}

	return results, nil
}

type senateVoteXml struct {
	Members []senateMemberXml `xml:"members>member"`
}

type senateMemberXml struct {
	LastName  string `xml:"last_name"`
	FirstName string `xml:"first_name"`
	Party     string `xml:"party"`
	Vote      string `xml:"vote_cast"`
}

type legislature struct {
	members [3]map[gopol.Politician]string
}

func (l legislature) Members(body int) []gopol.Politician {
	out := make([]gopol.Politician, 0, len(l.members[body]))
	for member, _ := range l.members[body] {
		out = append(out, member)
	}
	return out
}

func (l legislature) District(p gopol.Politician) string {
	if res, ok := l.members[0][p]; ok {
		return res
	}
	return l.members[1][p]
}

func (i *interface_) Legislature(num int) (gopol.LegislatureSession, error) {
	congress, err := i.legislature(num)
	if err != nil {
		return nil, err
	}

	res := legislature{}
	for i, chamber := range congress {
		mems := make(map[gopol.Politician]string)
		for _, member := range chamber {
			mems[member.Politician] = member.District
		}
		res.members[i] = mems
	}
	return res, nil
}

func (i *interface_) legislature(num int) ([][]legislator, error) {
	if res, ok := i.congresses[num]; ok {
		return res, nil
	}

	senators := make([]legislator, 0)
	representatives := make([]legislator, 0)
	delegates := make([]legislator, 0)

	page := 1
	for {
		resp, err := http.Get(fmt.Sprintf("https://www.congress.gov/members?q={\"congress\":%d}&page=%d", num, page))
		if err != nil {
			return nil, err
		}
		node, err := html.Parse(resp.Body)
		if err != nil {
			return nil, err
		}

		getAttr := func(n *html.Node, attr string) string {
			for _, attrdata := range n.Attr {
				if attrdata.Key == attr {
					return attrdata.Val
				}
			}
			return ""
		}

		var findMain func(n *html.Node) (*html.Node, bool)
		findMain = func(n *html.Node) (*html.Node, bool) {
			if n.Type == html.ElementNode && getAttr(n, "id") == "main" {
				return n, true
			}

			for c := n.FirstChild; c != nil; c = c.NextSibling {
				if res, ok := findMain(c); ok {
					return res, true
				}
			}
			return nil, false
		}

		mainElem, ok := findMain(node)
		if !ok {
			return nil, fmt.Errorf("internal error: couldn't find main elem")
		}

		olElem := (*html.Node)(nil)
		for c := mainElem.FirstChild; c != nil; c = c.NextSibling {
			if c.Type == html.ElementNode && c.Data == "ol" {
				olElem = c
				break
			}
		}

		if olElem == nil {
			return nil, gopol.ErrNotAvailable
		}

		for c := olElem.FirstChild; c != nil; c = c.NextSibling {
			if c.Type == html.ElementNode && c.Data == "li" && getAttr(c, "class") == "expanded" {
				liElem := c

				fullName := ""
				party := ""
				state := ""
				district := ""

				for c := liElem.FirstChild; c != nil; c = c.NextSibling {
					// .result-heading
					if c.Type == html.ElementNode && getAttr(c, "class") == "result-heading" {
						anchor := c.FirstChild
						for anchor != nil && !(anchor.Type == html.ElementNode && anchor.Data == "a") {
							anchor = anchor.NextSibling
						}
						fullName = anchor.FirstChild.Data
					}

					// .quick-search-member
					if c.Type == html.ElementNode && getAttr(c, "class") == "quick-search-member" {
						// .member-profile
						memberProfile := c.FirstChild
						for memberProfile != nil && !(memberProfile.Type == html.ElementNode && strings.Contains(getAttr(memberProfile, "class"), "member-profile")) {
							memberProfile = memberProfile.NextSibling
						}

						// each .result-item
						for resultItem := memberProfile.FirstChild; resultItem != nil; resultItem = resultItem.NextSibling {
							if resultItem.Type == html.ElementNode {
								label := ""
								data := ""
								for c := resultItem.FirstChild; c != nil; c = c.NextSibling {
									if c.Type == html.ElementNode && c.Data == "strong" {
										label = c.FirstChild.Data
									}
									if c.Type == html.ElementNode && c.Data == "span" {
										data = c.FirstChild.Data
									}
								}

								if strings.Contains(label, "Party") {
									if data == "Independent" {
										party = ""
									} else {
										party = string([]byte{data[0]})
									}
								}

								if strings.Contains(label, "State") {
									state = data
								}
								if strings.Contains(label, "District") {
									district = data
								}
							}
						}
					}
				}

				type_ := ""
				if strings.HasPrefix(fullName, "Representative") {
					type_ = "representative"
					fullName = strings.Trim(fullName[len("Representative"):], " \t")
				}
				if strings.HasPrefix(fullName, "Senator") {
					type_ = "senator"
					fullName = strings.Trim(fullName[len("Senator"):], " \t")
				}
				if strings.HasPrefix(fullName, "Delegate") {
					type_ = "delegate"
					fullName = strings.Trim(fullName[len("Delegate"):], " \t")
				}

				nameParts := strings.Split(fullName, ",")
				if len(nameParts) == 2 {
					fullName = strings.Trim(nameParts[1], " \t") + " " + strings.Trim(nameParts[0], " \t")
				}
				if len(nameParts) == 3 {
					fullName = strings.Trim(nameParts[1], " \t") + " " + strings.Trim(nameParts[0], " \t") + " " + strings.Trim(nameParts[2], " \t")
				}

				fullDistrict := statesRev[state]
				if district != "" {
					fullDistrict += "-" + district
				}

				politician := legislator{Politician: gopol.Politician{Name: fullName, Party: party}, District: fullDistrict}
				if type_ == "senator" {
					senators = append(senators, politician)
				} else if type_ == "representative" {
					representatives = append(representatives, politician)
				} else {
					delegates = append(delegates, politician)
				}
			}
		}

		// Determine if its the last page
		var findSearchTune func(n *html.Node) (*html.Node, bool)
		findSearchTune = func(n *html.Node) (*html.Node, bool) {
			if n.Type == html.ElementNode && getAttr(n, "id") == "searchTune" {
				return n, true
			}

			for c := n.FirstChild; c != nil; c = c.NextSibling {
				if res, ok := findSearchTune(c); ok {
					return res, true
				}
			}
			return nil, false
		}

		searchTune, ok := findSearchTune(node)
		if !ok {
			break
		}

		searchTuneNumber := searchTune.FirstChild
		for searchTuneNumber != nil && !(searchTuneNumber.Type == html.ElementNode && getAttr(searchTuneNumber, "class") == "basic-search-tune-number") {
			searchTuneNumber = searchTuneNumber.NextSibling
		}

		pagination := searchTuneNumber.FirstChild
		for pagination != nil && !(pagination.Type == html.ElementNode && getAttr(pagination, "class") == "pagination") {
			pagination = pagination.NextSibling
		}

		resultsNumber := pagination.FirstChild
		for resultsNumber != nil && !(resultsNumber.Type == html.ElementNode && getAttr(resultsNumber, "class") == "results-number") {
			resultsNumber = resultsNumber.NextSibling
		}

		text := strings.Trim(resultsNumber.FirstChild.Data, " \t")
		if text == fmt.Sprintf("of %d", page) {
			break
		}

		page += 1
	}

	i.congresses[num] = [][]legislator{representatives, senators, delegates}

	return i.congresses[num], nil
}

func (i *interface_) Executive(num int) (gopol.ExecutiveSession, error) {
	panic("unsupported operation")
}
